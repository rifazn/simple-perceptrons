#!/usr/bin/env python3
import random
import numpy as np
from math import exp

"""
Author: Rifaz Nahiyan
GL: @rifazn

Impelements a perceptron that trains to become a NAND Gate.

Truth table for NAND Gate:

    +--------+-----+
    | Input  | Out |
    +--------+-----+
    | x1  x2 |  o  |
    +--------+-----+
    | 0   0  |  1  |
    | 0   1  |  1  |
    | 1   0  |  1  |
    | 1   1  |  0  |
    +--------+-----+

Perceptron logic:
    if w1 * x1 + w2 * x2 + bias > 0
        return 1
    else
        return 0
"""

def gen_data(size: int = 100) -> list:
    l = []
    for i in range(size):
        x1, x2 = (random.randrange(0, 2), random.randrange(0, 2))
        out = int(not x1 & x2)
        l.append([x1, x2, out])
    return l

def sigmoid(x: list):
    l = [exp(-i) for i in x]
    l = np.array(l)
    return 1 / (1 + l)

def main():
    weight1, weight2 = 4, 3
    bias = 10
    alpha = 0.1
    iters = 10000

    data = np.array(gen_data(100))    
    x1 = data[:, 0]
    x2 = data[:, 1]
    y = data[:, 2]

    for _ in range(iters):
        pred_y = weight1*x1 + weight2*x2 + bias
        pred_y = sigmoid(pred_y)
        slope_wrt_w1 = np.dot((y - pred_y), -x1)
        slope_wrt_w2 = np.dot((y - pred_y), -x2)
        slope_wrt_bias = np.dot((y - pred_y), np.array([-1] * len(x1)))

        weight1 -= alpha*slope_wrt_w1
        weight2 -= alpha*slope_wrt_w2
        bias -= alpha*slope_wrt_bias

    pred_y = weight1*x1 + weight2*x2 + bias
    pred_y = sigmoid(pred_y)
    print(f"w1: {weight1} \t w2: {weight2} \t bias: {bias}")
    print(y - pred_y)

if __name__ == "__main__":
    main()
