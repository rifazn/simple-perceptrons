# Description

Impelements a perceptron that trains using _Linear Regression_ to become a NAND Gate.

Input: `gen_data(size)` function returns a list of valid NAND logic dataset. Default: size = 100

The number of iterations can be changed by changing the `iters` variable.

Truth table for NAND Gate:

    +--------+-----+
    | Input  | Out |
    +--------+-----+
    | x1  x2 |  o  |
    +--------+-----+
    | 0   0  |  1  |
    | 0   1  |  1  |
    | 1   0  |  1  |
    | 1   1  |  0  |
    +--------+-----+

Perceptron logic:
    if w1 * x1 + w2 * x2 + bias > 0
        return 1
    else
        return 0

# Summary

After an iterating a total of 10000 times, it finds weights that can correctly
equate a two input NAND Gate logic.

